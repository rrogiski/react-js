import React, {useState} from 'react';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import StreamList from "./StreamList";
import StreamDetail from "./StreamDetail";
import Form from "./Form";

const movieList = [
    {
        movieId: 1,
        movieName: "The Godfather",
        leadActor: "Marlon Brando",
        leadActree: 'Diane Keaton',
        releaseYear: 1972,
        language: "English",
        collections: "Crime, Drama"
    },
    {
        movieId: 2,
        movieName: "The Shawshank Redemption",
        leadActor: "Tim Robbins",
        leadActree: '-',
        releaseYear: 1994,
        language: "English",
        collections: "Drama"
    },
    {
        movieId: 3,
        movieName: "Schindler's List",
        leadActor: "Liam Neeson",
        leadActree: 'Caroline Goodall',
        releaseYear: 1993,
        language: "English",
        collections: "Biography, Drama, History"
    },
    {
        movieId: 4,
        movieName: "Raging Bull",
        leadActor: "Robert De Niro",
        leadActree: 'Cathy Moriarty',
        releaseYear: 1980,
        language: "English",
        collections: "Biography, Drama, Sport"
    },
    {
        movieId: 5,
        movieName: "Casablanca",
        leadActor: "Humphrey Bogart",
        leadActree: 'Ingrid Bergman',
        releaseYear: 1942,
        language: "English",
        collections: "Drama, Romance, War"
    },

];

export const AppContext = React.createContext();

const App = () => {
    const [items, updateItems] = useState(movieList);

    const onDeleteClick = (id) => {
        const updatedItems = items.filter(({movieId}) => movieId !== id);
        updateItems(updatedItems);
    };

    const onAddClick = (item) => {
        const maxId = Math.max(...items.map(o => o.movieId), 1);

        const updatedItems = [...items, { ...item,
            movieId: maxId+1}];

        updateItems(updatedItems);
    };

    return (
        <AppContext.Provider value={{items, onDeleteClick, onAddClick}} >
            <Router>
                <Switch>
                    <Route exact path="/">
                        <StreamList />
                    </Route>
                    <Route path="/addMovie">
                        <Form />
                    </Route>
                    <Route path="/:movieId">
                        <StreamDetail />
                    </Route>
                </Switch>
            </Router>
        </AppContext.Provider>
    );

};

export default App;
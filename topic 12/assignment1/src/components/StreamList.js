import React, {useContext} from 'react';
import {AppContext} from "./App";
import {useHistory} from "react-router-dom";

const StreamList = () => {
    const {items, onDeleteClick} = useContext(AppContext);
    const history = useHistory();

    const handleClick = (path) => {
        history.push(path);
    };

    const deleteItem = (id) => {
        onDeleteClick(id);
    };

    return (
        <>
            <div>
                <h2 className="ui header">
                    List of Movies
                </h2>

                <button className="ui blue circular plus icon button" onClick={()=>{handleClick(`/addMovie`)}}>
                    <i className="plus icon"></i>
                </button>
            </div>

            <div className="ui divider"/>

            <div className="ui segment">
                <div className="ui three column stackable center aligned grid">
                    <div className="middle aligned row">
                    {items.map(({movieId, movieName, leadActor, leadActree, releaseYear, language, collections }) => {
                        return (
                            <div className="ui raised segment column" key={movieId}>
                                <h3 className="ui header">{movieName}</h3>
                                <p><b>Movie ID: </b>{movieId}</p>
                                <p><b>Lead Actor: </b>{leadActor}</p>
                                <p><b>Lead Actree: </b>{leadActree}</p>
                                <p><b>Year of Release: </b>{releaseYear}</p>
                                <p><b>Language: </b>{language}</p>
                                <p><b>Collections: </b>{collections}</p>
                                <button className="ui blue basic button" onClick={()=>{handleClick(`/${movieId}`)}}>
                                    Details
                                </button>
                                <button className="ui red basic button" onClick={() => {deleteItem(movieId);}}>
                                    Discard
                                </button>
                            </div>
                        );
                    })}
                    </div>
                </div>
            </div>
        </>
    );
};

export default StreamList;
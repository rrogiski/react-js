import React, {useContext, useState} from 'react';
import Field from "./Field";
import {AppContext} from "./App";
import {useHistory} from "react-router-dom";

const Form = () => {
    const fields = [
        {
            name: 'movieName',
            label: "Movie Name",
            inputType: "text",
            value: ''
        },
        {
            name: 'leadActor',
            label: "Lead Actor",
            inputType: "text",
            value: ''
        },
        {
            name: 'leadActree',
            label: "Lead Actress",
            inputType: "text",
            value: ''
        },
        {
            name: 'releaseYear',
            label: "Year of Release",
            inputType: "text",
            value: ''
        },
        {
            name: 'language',
            label: "Language",
            inputType: "text",
            value: ''
        },
        {
            name: 'collections',
            label: "Collections",
            inputType: "text",
            value: ''
        }
    ];

    const [elements, updateElements] = useState(fields);

    const {onAddClick} = useContext(AppContext);

    const onValueChange = (event, name) => {
        updateElements(elements.map(el => (el.name === name ? {...el, value: event.target.value} : el)));
    };

    const history = useHistory();

    const changeRoute = (path) => {
        history.push(path);
    };

    const onFormSubmit = event => {
        event.preventDefault();
        const newElement = elements.reduce((acc, currentValue) => {
            return {
                ...acc,
                [currentValue.name]: currentValue.value
            };
        }, {});

        onAddClick(newElement);
        changeRoute("/");
    };

    return (
        <>
            <h2 className="ui header">
                Add a Movie
            </h2>
            <button className="ui basic blue button" onClick={()=>{changeRoute(`/`)}}>
                <i className="icon home"></i>
                 Home
            </button>


            <div className="ui segment">
                <form onSubmit={onFormSubmit} className="ui form">
                    <div className="ui three column stackable center aligned grid">
                        <div className="middle aligned row">
                            <div className="ui raised segment column">
                                {elements.map((item) => <Field key={item.label} {...item} onValueChange={onValueChange}/>)}
                            </div>
                        </div>
                    </div>
                    <button className="ui primary button" type="submit">
                        {"Submit"}
                    </button>
                </form>
            </div>
        </>
    );

};

export default Form;
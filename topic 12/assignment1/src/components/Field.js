import React from "react";

const Field = ({name, label, inputType, value, onValueChange}) => {
    const onInputChange = (event) => {
        onValueChange(event, name);
    }

    return (
        <div className="field">
            <label>{label}</label>
            <input
                name = {name}
                type={inputType}
                value={value}
                onChange={(event) => {onInputChange(event);}}
            />
        </div>
    );
}

export default Field;
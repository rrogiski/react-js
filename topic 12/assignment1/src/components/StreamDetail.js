import React, {useContext} from 'react';
import {AppContext} from "./App";
import {useHistory, useParams} from "react-router-dom";

const StreamDetail = () => {
    const {items} = useContext(AppContext);

    const history = useHistory();

    let { movieId } = useParams();

    const movie = items.find((item)=>item.movieId === Number(movieId));

    const handleClick = (path) => {
        history.push(path);
    };


    return (
        <>
            <h2 className="ui header">
                Details of {movie.movieName}
            </h2>
            <button className="ui basic blue button" onClick={()=>{handleClick(`/`)}}>
                <i className="icon home"></i>
                Home
            </button>

            <div className="ui divider"/>

            <div className="ui segment">
                <div className="ui three column stackable center aligned grid">
                    <div className="middle aligned row">
                        <div className="ui raised segment column" key={movie.movieId}>
                            <h3 className="ui header">{movie.movieName}</h3>
                            <p><b>Movie ID: </b>{movie.movieId}</p>
                            <p><b>Lead Actor: </b>{movie.leadActor}</p>
                            <p><b>Lead Actree: </b>{movie.leadActree}</p>
                            <p><b>Year of Release: </b>{movie.releaseYear}</p>
                            <p><b>Language: </b>{movie.language}</p>
                            <p><b>Collections: </b>{movie.collections}</p>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default StreamDetail;
## Description
This series of projects contains the code solution to the React JS training assignments.
It is organized by topic and assingment number.

Each project contains details on how to run each React JS program.

## Evidences
Each assigment project contains a folder named "screenshots". This folder contains a screen capture of the browser while executing the application.

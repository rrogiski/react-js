import React from "react";
import PropTypes from 'prop-types';

class Button extends React.Component {
    render() {
        const {label} = this.props;
        return (
            <button type = "submit" className="ui primary button">
                {label}
            </button>
        )
    }
}

Button.propType = {
    label: PropTypes.string.isRequired
};

Button.defaultProps = {
    label: 'Submit'
}

export default Button;
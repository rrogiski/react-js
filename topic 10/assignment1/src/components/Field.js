import React from "react";
import PropTypes from 'prop-types';

class Field extends React.Component {

    onInputChange = (event) => {
        this.props.onInputChange(event, this.props.name);
    }

    render() {

        const {name, label, inputType} = this.props;
        return (
            <div className="field">
                <label>{label}</label>
                <input
                    name = {name}
                    type={inputType}
                    value={this.props.value}
                    onChange={this.onInputChange}
                    min={0}
                    max={10}
                    step="any"
                />
            </div>
        );
    }
}

Field.propTypes ={
    label: PropTypes.string.isRequired,
    inputType: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    onInputChange: PropTypes.func.isRequired
};

Field.defaultProps = {
    label: '',
    inputType: "number",
    value: '',
    onInputChange: undefined
};

export default Field;
import React from 'react';
import Field from "./Field";
import Button from "./Button";

class App extends React.Component {

    state = { fields:
        [
            {
                name: 'physics',
                label: "Physics' Mark",
                inputType: "number",
                value: ''
            },
            {
                name: 'chemistry',
                label: "Chemistry's Mark",
                inputType: "number",
                value: ''
            },
            {
                name: 'biology',
                label: "Biology's Mark",
                inputType: "number",
                value: ''
            },
            {
                name: 'mathematics',
                label: "Mathematics' Mark",
                inputType: "number",
                value: ''
            }
        ]};


    onInputChange = (event, name) => {
        this.setState({
            fields: this.state.fields.map(el => (el.name === name ? {...el, value: event.target.value} : el))
        });
    };

    onFormSubmit = event => {
        event.preventDefault();
        const average = this.state.fields.reduce((acc, currentValue) => acc+Number(currentValue.value), 0) / this.state.fields.length;
        window.alert(`Average Mark: ${average}`);
    };

    render(){
        return (
            <div className="ui segment">
                <form onSubmit={this.onFormSubmit} className="ui form">
                    {this.state.fields.map((item) => <Field key={item.label} {...item} onInputChange={this.onInputChange}/>)}

                    <Button label="Submit" />
                </form>
            </div>
        )
    }
}

export default App;
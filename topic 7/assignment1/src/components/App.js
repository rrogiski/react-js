import React from "react";
import PropTypes from 'prop-types';
import Header from "./Header";

class App extends React.Component {

  render(){
    const {name, preferredCities, age} = this.props;
    return (
        <>
          <Header heading={name} />
          <Header heading={age} />
          {preferredCities.map((city, index)=>{
              return (
                  <Header key={index} heading={city} />
              );
          })}
        </>
    )
  }
}

const isAgeValid = function(props, propName, componentName) {

    const isValid = props[propName] >= 18 && props[propName] <= 60;
    if (!isValid) {
        return new Error(`Invalid prop ${propName} passed to ${componentName}. Expected a value on the following interval: 18 <= age <= 60`);
    }
}

App.propTypes = {
    name: PropTypes.string.isRequired,
    preferredCities: PropTypes.arrayOf(PropTypes.string),
    age: isAgeValid
};

App.defaultProps = {
    name: "Steve",
    preferredCities: ["Bangalore", "Chennai"],
    age: 18
};

export default App;

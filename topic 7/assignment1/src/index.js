import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';

const params = {
    name: "Rosana",
    preferredCities: ["Curitiba", "Brasilia", "Gramado"],
    age: 26
};

ReactDOM.render(
    <App {...params} />,
    document.getElementById('root')
);

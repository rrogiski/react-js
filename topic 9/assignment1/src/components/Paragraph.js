import PropTypes from 'prop-types';
import React from 'react';

class Paragraph extends React.Component{

    UNSAFE_componentWillMount(){
        console.log("The component will mount");
    }

    componentDidMount(){
        console.log("The component did mount");
    }

    componentWillUnmount() {
        console.log("The component will unmount");
    }

    render() {
        const {text, handleClick} = this.props;
        return (
            <div className="ui padded container" onClick={handleClick} id="myQuote">
                <p className="ui header">{text}</p>
            </div>
        );
    }
}


Paragraph.propTypes = {
    text: PropTypes.string.isRequired,
    handleClick: PropTypes.func.isRequired
};

Paragraph.defaultProps = {
    text: "Have a nice day!",
    handleClick: undefined
};

export default Paragraph;

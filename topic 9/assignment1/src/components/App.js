import React from 'react';
import Faker from 'faker';
import {unmountComponentAtNode, findDOMNode} from "react-dom";
import Paragraph from "./Paragraph";

class App extends React.Component {

    handleClick = () => {
        unmountComponentAtNode(findDOMNode(this).parentNode);
    };

    render(){
        const {hacker} = Faker;
        return (
            <Paragraph text={hacker.phrase()} handleClick={this.handleClick} />
        )
    }
}

export default App;
import React from 'react';
import Faker from 'faker';
import Header from "./Header";
import Details from "./Details";


class Main extends React.Component {

     values = Array.apply(null, {length: 5}).map(Number.call, Number).map(()=>{
         const {name, random} = Faker;

         return {
             studentId: random.number(),
             studentName: name.findName(),
             studentMarks: Number(random.float() % 10).toFixed(2)
         };
     });

     titles = ['Student ID', 'Student Name', "Student Marks"];

    state = {
        students: this.values,
        titles: this.titles
    };

    render(){
        const {students, titles} = this.state;
        return (
            <div>
                <Header heading={"Student Details"} />
                <Details titles = {titles} values = {students}/>
            </div>
        )
    }
}

export default Main;
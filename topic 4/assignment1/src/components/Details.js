import React from "react";

class Details extends React.Component{

    renderRows(){
        return(
            <tbody>
                {this.props.values.map((item) =>

                    {
                        return (
                            <tr key={item.studentId}>
                                {Object.entries(item).map(([key, value], index) =>
                                    <td key={key}>{value}</td>
                                )}
                            </tr>
                        )
                    }
                )}
            </tbody>
        )
    }

    renderHeader(){
        return (
            <thead>
                <tr>
                    {this.props.titles.map((title) => {
                            return (
                                <th key={title}>{title}</th>
                            )
                        }
                    )}
                </tr>
            </thead>
        )
    }

    render(){
        return (
            <table className="ui table">
               {this.renderHeader()}
               {this.renderRows()}
            </table>
        )

    }

}

export default Details;
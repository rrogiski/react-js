import React from "react";
import PropTypes from 'prop-types';

class Button extends React.Component {
    render() {
        const {label, onButtonClick} = this.props;
        return (
            <button className="ui primary button" onClick={onButtonClick}>
                {label}
            </button>
        )
    }
}

Button.propType = {
    label: PropTypes.string.isRequired,
    onButtonClick: PropTypes.func.isRequired
};

Button.defaultProps = {
    label: 'Click to generate Multiplication tables of 5',
    onButtonClick: undefined
}

export default Button;
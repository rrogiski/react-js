import React from 'react';
import Header from "./Header";
import Table from "./Table";
import Button from "./Button";

class App extends React.Component {

    state = {values: [], count: 0};

     multiplicationValue = 5;

     titles = ['Operation', 'Result'];

     onButtonClick = () => {
         this.populateArray(this.state.count + 1);
     };

     populateArray = (count) => {
         const values = Array.apply(null, {length: count}).map(Number.call, Number).map((value)=>{
             return {
                 id: value,
                 text: `${this.multiplicationValue} x ${value} = `,
                 total: this.multiplicationValue * value
             };
         });
         this.setState({values, count});
     }

    render(){
        return (
            <div className="ui padded container" >
                <Button label={'Click to generate Multiplication tables of 5'} onButtonClick={this.onButtonClick} />
                <Header heading={`Multiplication Table of ${this.multiplicationValue}`} />
                <Table titles = {this.titles} values = {this.state.values}/>
            </div>
        )
    }
}

export default App;
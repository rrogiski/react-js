import React from 'react';
import ReactDOM from 'react-dom';


const App = () => {
    return (
        <div>
            <h2>Happy Learning-React</h2>
        </div>
    );
};

ReactDOM.render(
    <App />,
    document.querySelector('#root')
);



import React from "react";
import PropTypes from 'prop-types';
import Header from "./Header";

class App extends React.Component {

  render(){
    const {companyName, companyLocation} = this.props;
    return (
        <>
          <Header heading={companyName} />
          <Header heading={companyLocation} />
        </>
    )
  }
}

App.propTypes = {
  companyName: PropTypes.string,
  companyLocation: PropTypes.string
};

App.defaultProps = {
  companyName: "WIPRO",
  companyLocation: "BANGALORE"
};

export default App;

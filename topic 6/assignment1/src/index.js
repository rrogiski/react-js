import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';

const params = {
    companyName: "Microsoft",
    companyLocation: "The United States of America"
};

ReactDOM.render(
    <App {...params} />,
    document.getElementById('root')
);

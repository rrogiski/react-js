import React from 'react';
import Faker from 'faker';
import Header from "./Header";
import Table from "./Table";

class App extends React.Component {
     employees = Array(5).fill().map(()=>{
         const { internet, name, random } = Faker;

         return {

             id: `e${random.number()}`,
             name: name.findName(),
             email: internet.email()
         };
     });

     titles = ['Employee ID', 'Employee Name', 'Employee E-mail']


    render(){
        return (
            <div>
                <Header heading="List of Employees" />
                <Table titles = {this.titles} employees = {this.employees}/>
            </div>
        )
    }
}

export default App;
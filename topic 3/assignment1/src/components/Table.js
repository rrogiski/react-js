import React from "react";

class Table extends React.Component{

    renderRows(){
        return(
            <tbody>
                {this.props.employees.map(({id, name, email}) =>
                    {
                        return (
                            <tr key={id}>
                                <td>{id}</td>
                                <td>{name}</td>
                                <td>{email}</td>
                            </tr>
                        )
                    }
                )}
            </tbody>
        )

    }

    renderHeader(){
        return (
            <thead>
                <tr>
                    {this.props.titles.map((title) => {
                            return (
                                <th key={title}>{title}</th>
                            )
                        }
                    )}
                </tr>
            </thead>
        )
    }

    render(){
        return (
            <table className="ui celled table">
               {this.renderHeader()}
                {this.renderRows()}
            </table>
        )

    }


}

export default Table;
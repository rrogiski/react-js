import React from 'react';

const Header = ({heading}) => {
    return (
        <h1 className="ui header">{heading}</h1>
    );
};

export default Header;
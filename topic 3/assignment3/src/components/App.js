import React from 'react';
import Header from "./Header";
import Table from "./Table";

class App extends React.Component {
     multiplicationValue = 5;
     values = Array.apply(null, {length: 11}).map(Number.call, Number).map((value)=>{
         return {
             id: value,
             text: `${this.multiplicationValue} x ${value} = `,
             total: this.multiplicationValue * value
         };
     });

     titles = ['Operation', 'Result']


    render(){
        return (
            <div>
                <Header heading={`Multiplication Table of ${this.multiplicationValue}`} />
                <Table titles = {this.titles} values = {this.values}/>
            </div>
        )
    }
}

export default App;
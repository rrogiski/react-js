import React from "react";

class Table extends React.Component{

    renderRows(){
        return(
            <tbody>
                {this.props.values.map((item) =>

                    {
                        return (
                            <tr key={item.id}>
                                {Object.entries(item).filter(([key]) =>
                                    key !== 'id'
                                ).map(([key, value], index) =>
                                    <td className={index === 0 ? 'right aligned' : ''} key={value}>{value}</td>
                                )}
                            </tr>
                        )
                    }
                )}
            </tbody>
        )
    }

    renderHeader(){
        return (
            <thead>
                <tr>
                    {this.props.titles.map((title) => {
                            return (
                                <th key={title}>{title}</th>
                            )
                        }
                    )}
                </tr>
            </thead>
        )
    }

    render(){
        return (
            <table className="ui collapsing table">
               {this.renderHeader()}
               {this.renderRows()}
            </table>
        )

    }

}

export default Table;